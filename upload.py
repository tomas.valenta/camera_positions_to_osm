import os
from datetime import date
from osm_bulk_upload import upload


if os.path.isfile("result.diff.xml"):
    if input("Delete old result.diff.xml file? [y/n]: ") == "y":
        print("Deleting result.diff.xml.")
        os.remove("result.diff.xml")


with open("result.csv", "r") as source:
    source_csv = source.read()


POSITIONS = []

for line in iter(source_csv.splitlines()):
    if not line:
        continue

    position = line.split("\t")

    POSITIONS.append((position[0], position[1]))

result = """
    <osmChange version="0.6" generator="camera_pois_to_coords">
        <create>
    """
i = 0

with open("city_name.txt", "r") as city_name_file:
    city = city_name_file.read()
    city = city.replace("\n", "")

with open("source_name.txt", "r") as source_name_file:
    source_link = source_name_file.read()
    source_link = source_link.replace("\n", "")

for position in POSITIONS:
    lat, lon = position

    i += 1

    result += f"""
<node id="-{i}" lon="{lon}" lat="{lat}" version="0">
    <tag k="man_made" v="surveillance"/>
    <tag k="surveillance:type" v="camera"/>
    <tag k="surveillance" v="public"/>
    <tag k="surveillance:zone" v="town"/>
    <tag k="operator" v="Městská policie {city}"/>
</node>
"""

result += """
    </create>
</osmChange>
"""

print("Writing to result.osc")

with open("result.osc", "w") as open_file:
    open_file.write(result)

print("File written. Running upload.")

current_date = date.today()
comment = f"Manual import of surveillance camera POIs on {city} city website, date {current_date.isoformat()}"

upload.run(parameters=[
    "./result.osc",
    "-m",
    comment,
    "-y",
    source_link,
])