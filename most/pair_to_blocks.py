import json


with open("blocks.geojson", "r") as geojson_file:
    geojson = json.loads(geojson_file.read())


coordinates = {}


for feature in geojson["features"]:
    # Example feature:
    # { "type": "Feature", "properties": { "OBJECTID": 21, "BLOK": 8, "Shape_Length": 0.0011637582062111896, "Shape_Area": 5.5562216877898299e-08 }, "geometry": { "type": "MultiPolygon", "coordinates": [ [ [ [ 13.641880099000048, 50.514988327000026 ], [ 13.641792146000057, 50.514866315000063 ], [ 13.641666360000045, 50.514902781000046 ], [ 13.641754440000057, 50.515024805000053 ], [ 13.641842530000076, 50.515146831000038 ], [ 13.641930757000068, 50.515268869000067 ], [ 13.642055735000042, 50.515232238000067 ], [ 13.641967907000037, 50.51511032600007 ], [ 13.641880099000048, 50.514988327000026 ] ] ] ] } },

    geometry = feature["geometry"]

    coordinates[str(feature["properties"]["BLOK"])] = {"lat": None, "lon": None}
    coordinate_sets = []

    for coordinate_set in geometry["coordinates"][0][0]:
        coordinate_sets.append({
            "lon": coordinate_set[0],
            "lat": coordinate_set[1]
        })

    average_lon = 0
    average_lat = 0

    for coordinate_set in coordinate_sets:
        average_lon += coordinate_set["lon"]
        average_lat += coordinate_set["lat"]

    average_lon = average_lon / len(coordinate_sets)
    average_lat = average_lat / len(coordinate_sets)

    coordinates[str(feature["properties"]["BLOK"])].update({
        "lat": average_lat,
        "lon": average_lon
    })


source_blocks = []


with open("source_blocks.txt", "r") as source_blocks_file:
    source_blocks = source_blocks_file.read().split("\n")


with open("result.csv", "a") as found_locations:
    for source_block_id in source_blocks:
        if source_block_id not in coordinates:
            print(f"Block {source_block_id} not in coordinates")
            continue

        found_locations.write(f"{coordinates[source_block_id]['lat']}\t{coordinates[source_block_id]['lon']}\t{source_block_id}\n")
