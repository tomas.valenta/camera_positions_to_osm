import googlemaps

with open("key.txt", "r") as key_file:
    key = key_file.read()

key = key.replace("\n", "")

gmaps = googlemaps.Client(key=key)

city = input("City: ")
source_name = input("Source (website, including https:// etc.): ")
country = "Česká Republika"

pois = ""


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


with open("source.txt", "r") as source:
    parsed_source = source.read()

    for address in parsed_source.split("\n"):
        if address == "":
            continue

        address += f", {city}, {country}"
        location = None

        success = False

        for i in range(10):
            if success:
                break

            try:
                location = gmaps.geocode(address)
            except Exception as exception:
                print(bcolors.WARNING + f"Failed to get address {address} on attempt {i + 1}: {exception}, retrying..." + bcolors.ENDC)

                success = False
                continue

            success = True

        if len(location) == 0:
            print(bcolors.FAIL + f"Couldn't find: {address}" + bcolors.ENDC)

            continue

        location = location[0]

        if location['geometry']['location_type'] == 'APPROXIMATE':
            print(bcolors.FAIL + f"Approximate address only: {address}. Please add this manually in the exported CSV file." + bcolors.ENDC)

            pois += f"\t\t{address}\n"
        else:
            pois += f"{location['geometry']['location']['lat']}\t{location['geometry']['location']['lng']}\t{address}\n"


print(
    bcolors.OKCYAN
    + "Finished, writing to result.csv. Please check imported coordinates."
    + "\nUsually, the langitudes and longitudes shouldn't differ by more than 0.3 degrees."
    + bcolors.ENDC
)


with open("city_name.txt", "w") as city_name_file:
    city_name_file.write(city)


with open("source_name.txt", "w") as source_file:
    source_file.write(source_name)


with open("result.csv", "w") as result:
    result.write(pois)
