# Camera positions to OpenStreetMap POIs parser

Takes addresses (or, usually, approximate locations) of surveillance cameras in the Czech Republic, and parses them into machine readable POI coordinates. With a little work, this script could also work for any POI anywhere.

## Requirements

- PHP 7 or newer (for the bulk_upload.php script, whose Python version no longer functions)
- Python 3
- `googlemaps` package from PyPI (requirements.txt)
- An OpenStreetMap account
- A Google Maps API account - free for small scale use, requires the [Geocoding API](https://developers.google.com/maps/documentation/geocoding/requests-geocoding) to be enabled.

## Usage

- Insert your google maps API key into the `key.txt` file.
- Put your OSM username into `username.txt`, and the password into `password.txt`.
- When you're ready to import POIs, create the `source.txt` file with the contents formatted as such:

```
Potěminova
Svojsíkova
Hlavní nádraží
Městský úřad
Náplavka
```

- **Make sure the city doesn't already have manually added surveillance cameras**, at least not those which could potentially be the same as your ones. This is the case in many major cities.
- Run the `parse.py` script. Enter the city name and the URL to the webpage where you found the source data.
- **Check the results in the result.csv file.** If one or two sets of coordinates differ by more than 0.3 degrees or so, it is quite likely they're not imported correctly. This margin, of course, depends on how large the city you're importing data for is.
- Run the `upload.py` script to upload the data to OpenStreetMap.